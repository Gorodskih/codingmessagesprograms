﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuffmanCode
{
    class Node
    {
        public Node Right;
        public Node Left;
        public char Symbol;
        public int Frequency;
        public bool Orientation; //left - 0, right - 1

        public bool IsLeaf()
        {
            if ((this.Left == null) && (this.Right == null))
                return true;
            else return false;
        }

    }
}
