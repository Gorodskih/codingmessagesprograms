﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace HuffmanCode
{
    public partial class Form1 : Form
    {
        HuffmanTree CodeTree = new HuffmanTree();
        HuffmanTree DecodeTree = new HuffmanTree();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CodeTree.SetFrequencies(richTextBox1.Text);
            CodeTree.BuildTree();
            CodeTree.BuildCodeTable();
            CodeTree.ShowCodes(dataGridView1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox2.Text = CodeTree.EncodedString(richTextBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!checkBox1.Checked)
            {
                if (dataGridView2.Rows.Count == 1)
                {
                    MessageBox.Show("Не заполнена таблица частот", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                DecodeTree.SetFrequencies(dataGridView2);
                DecodeTree.BuildTree();
                DecodeTree.BuildCodeTable();
            }
            else DecodeTree.CodeTable = new Dictionary<char, BitArray>(CodeTree.CodeTable);
            DecodeTree.ShowCodes(dataGridView3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if ((checkBox1.Checked) && (CodeTree.CodeTable != null))
            {
                DecodeTree.CodeTable = new Dictionary<char, BitArray>(CodeTree.CodeTable);
            }
            DecodeTree.BuildDecodeTable();
            richTextBox4.Text = DecodeTree.DecodedString(richTextBox3.Text);
        }
    }
}
