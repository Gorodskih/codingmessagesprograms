﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HuffmanCode
{
    class HuffmanTree
    {
        public Node Root;
        private Dictionary<char, int> Frequencies;
        public Dictionary<char, BitArray> CodeTable;
        public Dictionary<string, char> DecodeTable;

        public void SetFrequencies(string source)
        {
            this.Frequencies = new Dictionary<char, int>();
            foreach (char symbol in source)
                if (this.Frequencies.ContainsKey(symbol))
                    this.Frequencies[symbol]++;
                else this.Frequencies.Add(symbol, 1);
            this.Frequencies = this.Frequencies.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public void SetFrequencies(DataGridView grid)
        {
            this.Frequencies = new Dictionary<char, int>();
            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row == grid.Rows[grid.Rows.Count - 1])
                {
                    this.Frequencies = this.Frequencies.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
                    return;
                }
                char tempSymbol = new Char();
                if (!Char.TryParse(row.Cells[0].Value.ToString(), out tempSymbol))
                {
                    MessageBox.Show("Неправильно введён символ: \"" + row.Cells[0].Value.ToString() + " \"", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                int tempFrequency = new int();
                if (!int.TryParse(row.Cells[1].Value.ToString(), out tempFrequency))
                {
                    MessageBox.Show("Неправильно введена частота: \"" + row.Cells[1].Value.ToString() + " \"", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                this.Frequencies.Add(tempSymbol, tempFrequency);
            }
            
        }

        public void BuildTree()
        {
            if (this.Frequencies == null)
            {
                DialogResult result = MessageBox.Show("Таблица частот пуста.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            List<Node> nodes = new List<Node>();

            foreach (KeyValuePair<char, int> symbol in Frequencies)
                nodes.Add(new Node {Symbol=symbol.Key, Frequency= symbol.Value});

            while (nodes.Count > 1)
            {
                nodes = nodes.OrderBy(node => node.Frequency).ToList<Node>();
                Node temp = new Node();
                temp.Left = nodes[0];
                temp.Right = nodes[1];
                temp.Left.Orientation = false;
                temp.Right.Orientation = true;
                temp.Frequency = temp.Left.Frequency + temp.Right.Frequency;
                nodes[1] = temp;
                nodes.RemoveAt(0);
            }

            this.Root = nodes[0];
        }

        public void BuildCodeTable()
        {
            if (this.Root == null)
            {
                DialogResult result = MessageBox.Show("Не сгенерировано дерево. Сгенерировать сейчас?", "Ошибка", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Retry) this.BuildTree();
                else return;
            }
            if (this.Root == null) return;
            this.CodeTable = new Dictionary<char, BitArray>();
            GetCode(new List<bool>(), Root.Left);
            GetCode(new List<bool>(), Root.Right);
        }

        private void GetCode(List<bool> PrevCode, Node node)
        {
            List<bool> Code = new List<bool>(PrevCode);
            Code.Add(node.Orientation);
            if (!node.IsLeaf())
            {
                if (node.Left != null)
                {
                    GetCode(Code, node.Left);
                }
                if (node.Right != null)
                {
                    GetCode(Code, node.Right);
                }
            }
            else
            {
                this.CodeTable.Add(node.Symbol, new BitArray(Code.ToArray()));
            }
        }

        public void ShowCodes(DataGridView grid)
        {
            grid.Rows.Clear();
            foreach (KeyValuePair<char, BitArray> code in this.CodeTable)
            {
                string tempstring = "";
                foreach (bool bit in code.Value)
                    if (bit)
                        tempstring += "1";
                    else tempstring += "0";
                grid.Rows.Add(code.Key.ToString(), tempstring);
            }
        }

        public string EncodedString(string source)
        {
            if (this.CodeTable == null)
            {
                DialogResult result =  MessageBox.Show("Не сгенерированы коды символов. Сгенерировать сейчас?", "Ошибка", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Retry) this.BuildCodeTable();
                else return "Ошибка";
            }

            {
                string OutString = "";
                foreach (char symbol in source)
                {
                    BitArray symbolcode;
                    this.CodeTable.TryGetValue(symbol, out symbolcode);
                    foreach (bool bit in symbolcode)
                        if (bit)
                            OutString += "1";
                        else OutString += "0";
                }
                return OutString;
            }
        }

        public void BuildDecodeTable()
        {
            if (this.CodeTable == null)
            {
                DialogResult result = MessageBox.Show("Не сгенерированы коды символов. Сгенерировать сейчас?", "Ошибка", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Retry) this.BuildCodeTable();
                else return;
            }
            if (this.CodeTable == null) return;
            this.DecodeTable = new Dictionary<string, char>();
            foreach (KeyValuePair<char, BitArray> kod in this.CodeTable)
            {
                string temp = "";
                foreach (bool bit in kod.Value)
                    if (bit == true)
                        temp += "1";
                    else temp += "0";
                this.DecodeTable.Add(temp, kod.Key);
            }

        }

        public string DecodedString(string source)
        {
            if (this.DecodeTable == null)
            {
                DialogResult result = MessageBox.Show("Не сгенерирована таблица декодирования. Сгенерировать сейчас?", "Ошибка", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Retry) this.BuildDecodeTable();
                else return "Ошибка";
            }
            if (this.DecodeTable == null) return "Ошибка";
            string temp = "";
            string SymbolCode = "";
            foreach (char bit in source)
            {
                SymbolCode += bit;
                char symbol = new char();
                if (DecodeTable.TryGetValue(SymbolCode, out symbol))
                {
                    temp += symbol;
                    SymbolCode = "";
                }
            }
            return temp;
        }
    }
}
