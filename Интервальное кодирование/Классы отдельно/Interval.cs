﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RangeCoding
{
    class Interval
    {

        public UInt64 Low;
        public UInt64 High;

        public Interval()
        {

        }

        public Interval(Interval range)
        {
            this.Low = range.Low;
            this.High = range.High;
        }

        public Interval(UInt64 low, UInt64 high)
        {
            this.Low = low;
            this.High = high;
        }

    }
}
