﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RangeCoding
{
    public partial class Form1 : Form
    {
        RangeCoder coder = new RangeCoder();
        RangeCoder decoder = new RangeCoder();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            coder.SetRanges(textBox1.Text);
            coder.ShowRanges(dataGridView1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = coder.GetCodedString(textBox1.Text).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!checkBox1.Checked)
                decoder.SetRanges(dataGridView2, Convert.ToInt32(textBox4.Text));
            else decoder.RangeTable = coder.RangeTable;
            decoder.ShowRanges(dataGridView3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox5.Text = decoder.GetDecodedString(Convert.ToDecimal(textBox3.Text), Convert.ToInt32(textBox4.Text));
        }
    }
}
