﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RangeCoding
{
    class RangeCoder
    {
        private Interval CurrentRange;
        public Dictionary<char, Interval> RangeTable;

        public RangeCoder()
        {
            this.CurrentRange = new Interval(0, 1);
        }

        public void SetRanges(DataGridView grid, int sourceLength)
        {
            Dictionary<char, int> CharCount = new Dictionary<char, int>();
            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row == grid.Rows[grid.Rows.Count - 1])
                {
                    return;
                }
                char tempSymbol = new Char();
                if (!Char.TryParse(row.Cells[0].Value.ToString(), out tempSymbol))
                {
                    MessageBox.Show("Неправильно введён символ: \"" + row.Cells[0].Value.ToString() + " \"", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                int tempFrequency = new int();
                if (!int.TryParse(row.Cells[1].Value.ToString(), out tempFrequency))
                {
                    MessageBox.Show("Неправильно введена частота: \"" + row.Cells[1].Value.ToString() + " \"", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                CharCount.Add(tempSymbol, tempFrequency);
                this.RangeTable = new Dictionary<char, Interval>();
                Decimal nextlowvalue = 0;
                Decimal freqsumm = 0;
                foreach (KeyValuePair<char, int> symbol in CharCount)
                {
                    freqsumm += Convert.ToDecimal(symbol.Value) / sourceLength;
                    Interval temp = new Interval(nextlowvalue, freqsumm);
                    nextlowvalue = temp.High;
                    this.RangeTable.Add(symbol.Key, temp);
                }
            }

        }

        public void SetRanges(string source)
        {
            Dictionary<char, int> CharCount = new Dictionary<char, int>();
            foreach (char symbol in source)
                if (CharCount.ContainsKey(symbol))
                    CharCount[symbol]++;
                else CharCount.Add(symbol, 1);
            this.RangeTable = new Dictionary<char, Interval>();
            Decimal nextlowvalue = 0;
            Decimal freqsumm = 0;
            foreach (KeyValuePair<char, int> symbol in CharCount)
            {
                freqsumm += Convert.ToDecimal(symbol.Value) / source.Length;
                Interval temp = new Interval(nextlowvalue, freqsumm);
                nextlowvalue = temp.High;
                this.RangeTable.Add(symbol.Key,temp);
            }
        }

        public Decimal GetCodedString(string source)
        {
            this.CurrentRange.Low = 0;
            this.CurrentRange.High = 1;
            foreach (char symbol in source)
            {
                SetNewRange(symbol);
            }
            return this.CurrentRange.Low;
        }

        private void SetNewRange(char symbol)
        {
            Interval temp = new Interval();
            temp.Low = this.CurrentRange.Low + (this.CurrentRange.High - this.CurrentRange.Low) * this.RangeTable[symbol].Low;
            temp.High = this.CurrentRange.Low + (this.CurrentRange.High - this.CurrentRange.Low) * this.RangeTable[symbol].High;
            this.CurrentRange = temp;
        }

        public string GetDecodedString(Decimal messagecode, int length)
        {
            Decimal code = messagecode;
            string decodedstring = "";
            for (int i = 0; i < length; i++)
            {
                KeyValuePair<char, Interval> temp = FindRange(code);
                decodedstring += temp.Key;
                code = (code - temp.Value.Low) / (temp.Value.High - temp.Value.Low);
            }
            return decodedstring;
        }

        private KeyValuePair<char, Interval> FindRange(Decimal code)
        {
            foreach (KeyValuePair<char, Interval> range in this.RangeTable)
                if ((code >= range.Value.Low) && (code < range.Value.High)) return range;
            return new KeyValuePair<char, Interval>();
        }

        public void ShowRanges(DataGridView grid)
        {
            grid.Rows.Clear();
            foreach (KeyValuePair<char, Interval> range in this.RangeTable)
                grid.Rows.Add(range.Key, range.Value.Low, range.Value.High);
        }

    }
}
