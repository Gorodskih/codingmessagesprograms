﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RangeCoding
{
    class Interval
    {

        public Decimal Low;
        public Decimal High;

        public Interval()
        {

        }

        public Interval(Interval range)
        {
            this.Low = range.Low;
            this.High = range.High;
        }

        public Interval(Decimal low, Decimal high)
        {
            this.Low = low;
            this.High = high;
        }

    }
}
